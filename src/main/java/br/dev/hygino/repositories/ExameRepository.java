package br.dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.dev.hygino.entities.Exame;

public interface ExameRepository extends JpaRepository<Exame, Long> {

}
