package br.dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.dev.hygino.entities.Convenio;

public interface ConvenioRepository extends JpaRepository<Convenio, Long> {

}
