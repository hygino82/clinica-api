package br.dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.dev.hygino.dto.CidadeDTO;
import br.dev.hygino.dto.InserirCidadeDTO;
import br.dev.hygino.services.CidadeService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/cidade")
public class CidadeController {

	private final CidadeService service;

	public CidadeController(CidadeService service) {
		this.service = service;
	}

	@PostMapping
	public ResponseEntity<CidadeDTO> inserir(@RequestBody @Valid InserirCidadeDTO dto) {
		CidadeDTO cidadeDTO = service.inserir(dto);
		return ResponseEntity.status(201).body(cidadeDTO);
	}

	@GetMapping
	public ResponseEntity<Page<CidadeDTO>> buscarCidades(Pageable pageable) {
		Page<CidadeDTO> page = service.buscarCidades(pageable);
		return ResponseEntity.status(200).body(page);
	}

	@GetMapping("/{id}")
	public ResponseEntity<CidadeDTO> buscarCidadePorId(@PathVariable Long id) {
		CidadeDTO dto = service.buscarCidadePorId(id);
		return ResponseEntity.status(200).body(dto);
	}

	@PutMapping("/{id}")
	public ResponseEntity<CidadeDTO> editarCidade(@PathVariable Long id, @RequestBody @Valid InserirCidadeDTO dto) {
		CidadeDTO cidadeDTO = service.editarCidade(id, dto);
		return ResponseEntity.status(200).body(cidadeDTO);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removerVidade(@PathVariable Long id) {
		service.removerCidade(id);
		return ResponseEntity.noContent().build();
	}
}
