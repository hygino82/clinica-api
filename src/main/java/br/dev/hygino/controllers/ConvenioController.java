package br.dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.dev.hygino.dto.ConvenioDTO;
import br.dev.hygino.dto.InserirConvenioDTO;
import br.dev.hygino.services.ConvenioService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/convenio")
public class ConvenioController {
    private final ConvenioService service;

	public ConvenioController(ConvenioService service) {
		this.service = service;
	}

	@PostMapping
	public ResponseEntity<ConvenioDTO> inserir(@RequestBody @Valid InserirConvenioDTO dto) {
		ConvenioDTO ConvenioDTO = service.inserir(dto);
		return ResponseEntity.status(201).body(ConvenioDTO);
	}

	@GetMapping
	public ResponseEntity<Page<ConvenioDTO>> buscarConvenios(Pageable pageable) {
		Page<ConvenioDTO> page = service.buscarConvenios(pageable);
		return ResponseEntity.status(200).body(page);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ConvenioDTO> buscarConvenioPorId(@PathVariable Long id) {
		ConvenioDTO dto = service.buscarConvenioPorId(id);
		return ResponseEntity.status(200).body(dto);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ConvenioDTO> editarConvenio(@PathVariable Long id, @RequestBody @Valid InserirConvenioDTO dto) {
		ConvenioDTO ConvenioDTO = service.editarConvenio(id, dto);
		return ResponseEntity.status(200).body(ConvenioDTO);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removerVidade(@PathVariable Long id) {
		service.removerConvenio(id);
		return ResponseEntity.noContent().build();
	}
}
