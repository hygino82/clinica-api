package br.dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.dev.hygino.dto.ExameDTO;
import br.dev.hygino.dto.InserirExameDTO;
import br.dev.hygino.services.ExameService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/exame")
public class ExameController {
    private final ExameService service;

	public ExameController(ExameService service) {
		this.service = service;
	}

	@PostMapping
	public ResponseEntity<ExameDTO> inserir(@RequestBody @Valid InserirExameDTO dto) {
		ExameDTO ExameDTO = service.inserir(dto);
		return ResponseEntity.status(201).body(ExameDTO);
	}

	@GetMapping
	public ResponseEntity<Page<ExameDTO>> buscarExames(Pageable pageable) {
		Page<ExameDTO> page = service.buscarExames(pageable);
		return ResponseEntity.status(200).body(page);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ExameDTO> buscarExamePorId(@PathVariable Long id) {
		ExameDTO dto = service.buscarExamePorId(id);
		return ResponseEntity.status(200).body(dto);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ExameDTO> editarExame(@PathVariable Long id, @RequestBody @Valid InserirExameDTO dto) {
		ExameDTO ExameDTO = service.editarExame(id, dto);
		return ResponseEntity.status(200).body(ExameDTO);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removerVidade(@PathVariable Long id) {
		service.removerExame(id);
		return ResponseEntity.noContent().build();
	}
}
