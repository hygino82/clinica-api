package br.dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.dev.hygino.dto.EspecialidadeDTO;
import br.dev.hygino.dto.InserirEspecialidadeDTO;
import br.dev.hygino.services.EspecialidadeService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/especialidade")
public class EspecialidadeController {
    
    private final EspecialidadeService service;

	public EspecialidadeController(EspecialidadeService service) {
		this.service = service;
	}

	@PostMapping
	public ResponseEntity<EspecialidadeDTO> inserir(@RequestBody @Valid InserirEspecialidadeDTO dto) {
		EspecialidadeDTO EspecialidadeDTO = service.inserir(dto);
		return ResponseEntity.status(201).body(EspecialidadeDTO);
	}

	@GetMapping
	public ResponseEntity<Page<EspecialidadeDTO>> buscarEspecialidades(Pageable pageable) {
		Page<EspecialidadeDTO> page = service.buscarEspecialidades(pageable);
		return ResponseEntity.status(200).body(page);
	}

	@GetMapping("/{id}")
	public ResponseEntity<EspecialidadeDTO> buscarEspecialidadePorId(@PathVariable Long id) {
		EspecialidadeDTO dto = service.buscarEspecialidadePorId(id);
		return ResponseEntity.status(200).body(dto);
	}

	@PutMapping("/{id}")
	public ResponseEntity<EspecialidadeDTO> editarEspecialidade(@PathVariable Long id, @RequestBody @Valid InserirEspecialidadeDTO dto) {
		EspecialidadeDTO EspecialidadeDTO = service.editarEspecialidade(id, dto);
		return ResponseEntity.status(200).body(EspecialidadeDTO);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removerVidade(@PathVariable Long id) {
		service.removerEspecialidade(id);
		return ResponseEntity.noContent().build();
	}
}
