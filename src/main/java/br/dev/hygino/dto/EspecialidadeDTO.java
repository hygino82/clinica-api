package br.dev.hygino.dto;

import br.dev.hygino.entities.Especialidade;

public record EspecialidadeDTO(Long id, String nome) {
	public EspecialidadeDTO(Especialidade entity) {
		this(entity.getId(), entity.getNome());
	}
}
