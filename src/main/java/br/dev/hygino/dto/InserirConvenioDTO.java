package br.dev.hygino.dto;

import jakarta.validation.constraints.NotNull;

public record InserirConvenioDTO(@NotNull String nome) {

}
