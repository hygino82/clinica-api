package br.dev.hygino.dto;

import jakarta.validation.constraints.NotBlank;

public record InserirEspecialidadeDTO(@NotBlank String nome) {

}
