package br.dev.hygino.dto;

import jakarta.validation.constraints.NotBlank;

public record InserirExameDTO(@NotBlank String nome) {

}
