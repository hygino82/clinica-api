package br.dev.hygino.dto;

import br.dev.hygino.entities.Cidade;

public record CidadeDTO(Long id, String nome) {
	public CidadeDTO(Cidade entity) {
		this(entity.getId(), entity.getNome());
	}
}
