package br.dev.hygino.dto;

import br.dev.hygino.entities.Exame;

public record ExameDTO(Long id, String nome) {
    public ExameDTO(Exame entity) {
        this(entity.getId(), entity.getNome());
    }
}
