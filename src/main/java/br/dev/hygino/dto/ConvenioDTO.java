package br.dev.hygino.dto;

import br.dev.hygino.entities.Convenio;

public record ConvenioDTO(Long id, String nome) {
    public ConvenioDTO(Convenio entity) {
        this(entity.getId(), entity.getNome());
    }
}
