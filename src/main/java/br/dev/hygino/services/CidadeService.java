package br.dev.hygino.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.dev.hygino.dto.CidadeDTO;
import br.dev.hygino.dto.InserirCidadeDTO;
import br.dev.hygino.entities.Cidade;
import br.dev.hygino.repositories.CidadeRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class CidadeService {

	private final CidadeRepository repository;

	public CidadeService(CidadeRepository repository) {
		this.repository = repository;
	}

	@Transactional
	public CidadeDTO inserir(@Valid InserirCidadeDTO dto) {
		Cidade entity = repository.save(new Cidade(dto));
		return new CidadeDTO(entity);
	}

	@Transactional(readOnly = true)
	public Page<CidadeDTO> buscarCidades(Pageable pageable) {
		Page<Cidade> page = repository.findAll(pageable);
		return page.map(cidade -> new CidadeDTO(cidade));
	}

	@Transactional(readOnly = true)
	public CidadeDTO buscarCidadePorId(Long id) {
		Cidade entity = repository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Não existe cidade com o id: " + id));
		return new CidadeDTO(entity);
	}

	@Transactional
	public CidadeDTO editarCidade(Long id, InserirCidadeDTO dto) {
		try {
			Cidade entity = repository.getReferenceById(id);
			entity.setNome(dto.nome());
			entity = repository.save(entity);
			return new CidadeDTO(entity);
		} catch (EntityNotFoundException e) {
			throw new IllegalArgumentException("Não existe Cidade com o Id: " + id);
		}
	}

	public void removerCidade(Long id) {
		try {
			Cidade cidade = repository.getReferenceById(id);
			repository.delete(cidade);
		} catch (EntityNotFoundException e) {
			throw new IllegalArgumentException("Não existe Cidade com o Id: " + id);
		}
	}
}
