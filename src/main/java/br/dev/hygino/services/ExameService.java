package br.dev.hygino.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.dev.hygino.dto.ExameDTO;
import br.dev.hygino.dto.InserirExameDTO;
import br.dev.hygino.entities.Exame;
import br.dev.hygino.repositories.ExameRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class ExameService {
    private final ExameRepository repository;

	public ExameService(ExameRepository repository) {
		this.repository = repository;
	}

	@Transactional
	public ExameDTO inserir(@Valid InserirExameDTO dto) {
		Exame entity = repository.save(new Exame(dto));
		return new ExameDTO(entity);
	}

	@Transactional(readOnly = true)
	public Page<ExameDTO> buscarExames(Pageable pageable) {
		Page<Exame> page = repository.findAll(pageable);
		return page.map(Exame -> new ExameDTO(Exame));
	}

	@Transactional(readOnly = true)
	public ExameDTO buscarExamePorId(Long id) {
		Exame entity = repository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Não existe Exame com o id: " + id));
		return new ExameDTO(entity);
	}

	@Transactional
	public ExameDTO editarExame(Long id, InserirExameDTO dto) {
		try {
			Exame entity = repository.getReferenceById(id);
			entity.setNome(dto.nome());
			entity = repository.save(entity);
			return new ExameDTO(entity);
		} catch (EntityNotFoundException e) {
			throw new IllegalArgumentException("Não existe Exame com o Id: " + id);
		}
	}

	public void removerExame(Long id) {
		try {
			Exame Exame = repository.getReferenceById(id);
			repository.delete(Exame);
		} catch (EntityNotFoundException e) {
			throw new IllegalArgumentException("Não existe Exame com o Id: " + id);
		}
	}
}
