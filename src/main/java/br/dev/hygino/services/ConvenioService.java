package br.dev.hygino.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.dev.hygino.dto.ConvenioDTO;
import br.dev.hygino.dto.InserirConvenioDTO;
import br.dev.hygino.entities.Convenio;
import br.dev.hygino.repositories.ConvenioRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class ConvenioService {
    private final ConvenioRepository repository;

    public ConvenioService(ConvenioRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public ConvenioDTO inserir(@Valid InserirConvenioDTO dto) {
        Convenio entity = repository.save(new Convenio(dto));
        return new ConvenioDTO(entity);
    }

    @Transactional(readOnly = true)
    public Page<ConvenioDTO> buscarConvenios(Pageable pageable) {
        Page<Convenio> page = repository.findAll(pageable);
        return page.map(Convenio -> new ConvenioDTO(Convenio));
    }

    @Transactional(readOnly = true)
    public ConvenioDTO buscarConvenioPorId(Long id) {
        Convenio entity = repository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Não existe Convenio com o id: " + id));
        return new ConvenioDTO(entity);
    }

    @Transactional
    public ConvenioDTO editarConvenio(Long id, InserirConvenioDTO dto) {
        try {
            Convenio entity = repository.getReferenceById(id);
            entity.setNome(dto.nome());
            entity = repository.save(entity);
            return new ConvenioDTO(entity);
        } catch (EntityNotFoundException e) {
            throw new IllegalArgumentException("Não existe Convenio com o Id: " + id);
        }
    }

    public void removerConvenio(Long id) {
        try {
            Convenio Convenio = repository.getReferenceById(id);
            repository.delete(Convenio);
        } catch (EntityNotFoundException e) {
            throw new IllegalArgumentException("Não existe Convenio com o Id: " + id);
        }
    }
}
