package br.dev.hygino.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.dev.hygino.dto.EspecialidadeDTO;
import br.dev.hygino.dto.InserirEspecialidadeDTO;
import br.dev.hygino.entities.Especialidade;
import br.dev.hygino.repositories.EspecialidadeRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class EspecialidadeService {
    private final EspecialidadeRepository repository;

	public EspecialidadeService(EspecialidadeRepository repository) {
		this.repository = repository;
	}

	@Transactional
	public EspecialidadeDTO inserir(@Valid InserirEspecialidadeDTO dto) {
		Especialidade entity = repository.save(new Especialidade(dto));
		return new EspecialidadeDTO(entity);
	}

	@Transactional(readOnly = true)
	public Page<EspecialidadeDTO> buscarEspecialidades(Pageable pageable) {
		Page<Especialidade> page = repository.findAll(pageable);
		return page.map(Especialidade -> new EspecialidadeDTO(Especialidade));
	}

	@Transactional(readOnly = true)
	public EspecialidadeDTO buscarEspecialidadePorId(Long id) {
		Especialidade entity = repository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Não existe Especialidade com o id: " + id));
		return new EspecialidadeDTO(entity);
	}

	@Transactional
	public EspecialidadeDTO editarEspecialidade(Long id, InserirEspecialidadeDTO dto) {
		try {
			Especialidade entity = repository.getReferenceById(id);
			entity.setNome(dto.nome());
			entity = repository.save(entity);
			return new EspecialidadeDTO(entity);
		} catch (EntityNotFoundException e) {
			throw new IllegalArgumentException("Não existe Especialidade com o Id: " + id);
		}
	}

	public void removerEspecialidade(Long id) {
		try {
			Especialidade Especialidade = repository.getReferenceById(id);
			repository.delete(Especialidade);
		} catch (EntityNotFoundException e) {
			throw new IllegalArgumentException("Não existe Especialidade com o Id: " + id);
		}
	}
}